"use strict";

// Завдання 1
let paragraphs = document.querySelectorAll("p");
for (let element of paragraphs) {
  element.style.background = "#ff0000";
}

// Завдання 2
let elem = document.getElementById("optionsList");
console.log(elem);
let parent = elem.parentElement;
console.log(parent);
let nodes = elem.childNodes;
console.log(nodes);
for (let node of nodes) {
  console.log(node.nodeName, node.nodeType);
}

// Завдання 3
document.getElementById("testParagraph").innerHTML = "This is a paragraph";

// Завдання 4
let listLi = document.querySelector(".main-header").querySelectorAll("li");
console.log(listLi);
for (let elem of listLi) {
  elem.classList.add("nav-item");
}
console.log(listLi);

// Завдання 5
let elements = document.querySelectorAll(".section-title");
for (let element of elements) {
  element.classList.remove("section-title");
}
console.log(elements);
