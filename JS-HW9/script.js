"use strict";

function displeyArrList(arr, parent = document.body) {
  let newArr = arr.map(function (value) {
    let li = document.createElement("li");
    li.innerText = value;
    return li;
  });

  let ol = document.createElement("ol");
  ol.append(...newArr);

  parent.append(ol);
}

displeyArrList(["Перший", "Другий", "Третій", "Четвертий"]);
