"use strict";

let a = prompt("Введіть перше число");
let b = prompt("Введіть друге число");

while (isNaN(a) || isNaN(b) || !a || !b) {
  alert("Помилка! Число не введене");
  a = prompt("Введіть перше число", [a]);
  b = prompt("Введіть друге число", [b]);
}

let c = prompt("Введіть математичну операцію (+, -, *, /)");

console.log(getMathResult(+a, +b, c));

function getMathResult(a, b, c) {
  switch (c) {
    case "+":
      return a + b;
    case "-":
      return a - b;
    case "*":
      return a * b;
    case "/":
      return a / b;
    default:
      break;
  }
}
