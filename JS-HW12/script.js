"use strict";

function keyPainter() {
  document.addEventListener("keydown", function (event) {
    document.querySelectorAll(".btn").forEach((btn) => {
      btn.removeAttribute("style");
      if (
        event.key === btn.innerText ||
        event.key === btn.innerText.toLowerCase()
      ) {
        btn.style.backgroundColor = "blue";
      }
    });
  });
}
keyPainter();
