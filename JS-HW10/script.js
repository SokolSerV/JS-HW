"use strict";

function init() {
  let arrLiTitle = [];

  let liTitle = document.querySelectorAll(".tabs-title");
  liTitle.forEach(function (li) {
    li.setAttribute("data-title", li.innerText);

    arrLiTitle.push(li.innerText);

    li.addEventListener("click", clickTab);
  });

  let liContent = document.querySelectorAll(".tabs-content>li");
  for (let i = 0; i < liContent.length; i++) {
    liContent[i].style.display = "none";
    liContent[i].setAttribute("data-content", arrLiTitle[i]);
  }
  liContent[0].style.display = "";
}

function clickTab() {
  document.querySelector(".active").classList.remove("active");
  this.classList.add("active");

  let attributeClick = this.getAttribute("data-title");
  let contentClick = document.querySelector(
    `.tabs-content>li[data-content = ${attributeClick}]`
  );

  document.querySelector('.tabs-content>li[style = ""').style.display = "none";
  contentClick.style.display = "";
}

init();
