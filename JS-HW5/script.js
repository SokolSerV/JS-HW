"use strict";

function createNewUser() {
  let firstName = prompt("Enter firstName", ["First name"]);
  let lastName = prompt("Enter lastName", ["Last name"]);

  const newUser = {
    firstName: firstName,
    lastName: lastName,
    getLogin() {
      return (this.firstName[0] + this.lastName).toLowerCase();
    },
  };
  return newUser;
}
let user = createNewUser();
console.log(user.getLogin());
