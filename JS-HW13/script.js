"use strict";

let elemWrapper = document.querySelector(".images-wrapper");
let allImages = elemWrapper.children;
let firstImage = elemWrapper.firstElementChild;
let lastImage = elemWrapper.lastElementChild;

for (const image of allImages) {
  image.style.display = "none";
}
firstImage.style.display = "";

let timerId = setInterval(roundabout, 3000);

function roundabout() {
  let visibleImage = elemWrapper.querySelector("[style='']");
  // [style='display: none;']
  visibleImage.style.display = "none";
  if (visibleImage === lastImage) {
    firstImage.style.display = "";
  } else {
    visibleImage.nextElementSibling.style = "";
  }
}

let btnStop = document.createElement("button");
btnStop.innerHTML = "Припинити";
document.body.append(btnStop);
let btnPlay = document.createElement("button");
btnPlay.innerHTML = "Відновити показ";
document.body.append(btnPlay);
btnPlay.disabled = true;

btnStop.onclick = () => {
  clearInterval(timerId);
  btnStop.disabled = true;
  btnPlay.disabled = false;
};
btnPlay.onclick = () => {
  timerId = setInterval(roundabout, 3000);
  btnPlay.disabled = true;
  btnStop.disabled = false;
};
