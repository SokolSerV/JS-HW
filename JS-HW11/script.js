"use strict";

let form = document.querySelector(".password-form");
let icons = form.querySelectorAll("i");

for (const icon of icons) {
  icon.addEventListener("click", function (event) {
    event.target.classList.add("fa-eye");
    event.target.classList.toggle("fa-eye-slash");

    let label = event.target.closest(".input-wrapper");
    let input = label.querySelector("input");
    if (input.getAttribute("type") === "password") {
      input.type = "text";
    } else {
      input.type = "password";
    }
  });
}

form.addEventListener("submit", function (event) {
  event.preventDefault();

  let inputs = form.querySelectorAll("input");

  if (inputs[0].value === inputs[1].value) {
    alert("You are welcome");
  } else {
    let redText = document.createElement("span");
    redText.style.color = "red";
    redText.innerText = "Потрібно ввести однакові значення";
    let btn = document.querySelector(".btn");
    btn.before(redText);
    btn.setAttribute("disabled", "true");

    inputs.forEach((input) => {
      input.addEventListener("focus", function () {
        redText.remove();
        btn.removeAttribute("disabled");
      });
    });
  }
});
