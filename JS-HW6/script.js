"use strict";

function createNewUser() {
  let firstName = prompt("Enter firstName", ["firstName"]);
  let lastName = prompt("Enter lastName", ["lastName"]);
  let birthday = prompt("Enter birthday (dd.mm.yyyy)", ["dd.mm.yyyy"]);

  const newUser = {
    firstName: firstName,
    lastName: lastName,
    birthday: birthday,
    getLogin() {
      return (this.firstName[0] + this.lastName).toLowerCase();
    },
    getAge() {
      let str = birthday.split(".").reverse().join("-");
      let diff = new Date() - new Date(str);
      return Math.floor(diff / (1000 * 60 * 60 * 24 * 365));
    },
    getPassword() {
      return (
        this.firstName[0].toUpperCase() +
        this.lastName.toLowerCase() +
        birthday.slice(6)
      );
    },
  };
  return newUser;
}
let user = createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());
