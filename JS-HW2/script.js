let userName;
let userAge;
// let agreement;

do {
  userName = prompt("Enter your name", [userName]);
} while (userName === "" || userName === null);
do {
  userAge = prompt("Enter your age", [userAge]);
} while (userAge === "" || userAge === null || isNaN(userAge));

console.log(userName);
console.log(userAge);

if (userAge < 18) {
  alert("You are not allowed to visit this website");
} else if (userAge > 22) {
  alert("Welcome, " + userName);
} else {
  let agreement = confirm("Are you sure you want to continue?");

  console.log(agreement);

  if (agreement) {
    alert("Welcome, " + userName);
  } else {
    alert("You are not allowed to visit this website");
  }
  // agreement === true
  //   ? alert("Welcome, " + userName)
  //   : alert("You are not allowed to visit this website");
}
