"use strict";

function filterBy(arr, typeData) {
  let newArr = arr.filter((elValue) => typeof elValue !== typeData);
  return newArr;
}
let res = filterBy(
  ["hello", "world", 23, "23", null, true, false, 0, NaN, undefined],
  "number"
);
console.log(res);

// function filterBy(arr, typeData) {
//   let newArr = arr.filter(function (elValue) {
//     if (typeof elValue !== typeData) {
//       return elValue;
//     }
//   });
//   return newArr;
// }
// let res = filterBy(
//   ["hello", "world", 23, "23", null, true, false, 0, NaN, undefined],
//   "number"
// );
// console.log(res);

function filterBy(arr, typeData) {
  const newArr = [];
  arr.forEach(function (elValue) {
    if (typeof elValue !== typeData) {
      newArr.push(elValue);
    }
  });
  return newArr;
}
let res2 = filterBy(
  ["hello", "world", 23, "23", null, true, false, 0, NaN, undefined],
  "number"
);
console.log(res2);

// let a = "";
// function filterBy(arr, typeData) {
//   a = typeData;
//   let newArr = arr.filter(arrCbFn);
//   return newArr;
// }
// function arrCbFn(elValue) {
//   if (typeof elValue !== a) {
//     return elValue;
//   }
// }
// let res = filterBy(
//   ["hello", "world", 23, "23", null, true, false, 0, NaN, undefined],
//   "number"
// );
// console.log(res);
